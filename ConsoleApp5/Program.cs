﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            // veel mõned kasulikud asjad
            // JUHUSLIK ARV

            Random r = new Random();
            Console.WriteLine(r.Next()); // see on juhulik arv

            // kuidas leida juhuslik arv 0..100 vahemikus
            // kuidas teha nii, et 100 ka vahel tuleb ja 
            // kuidas teha nii, et 100 kunagi ei tule (0..99)
            // kuidas teha nii, et 0 ka kunagi ei tule

            while (true)  
            {
                Console.Write("MIllal sa sündinud oled: ");
                string vastus = Console.ReadLine();
                if (vastus == "") break;
                if (DateTime.TryParse(vastus, out DateTime sünnipäev))
                {

                    DateTime täna = DateTime.Now;
                    //Console.WriteLine(täna.DayOfYear);
                    //Console.WriteLine(
                    //(DateTime.Now - sünnipäev)
                    //);

                    int vanus = täna.Year - sünnipäev.Year;
                    if (sünnipäev.AddYears(vanus) > täna) vanus--;
                    Console.WriteLine($"vanus on {vanus}");
                }
                else Console.WriteLine("see pole õige sünnipäev");
            }
        }
    }
}
